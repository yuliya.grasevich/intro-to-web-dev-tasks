import './feature.css';
import { FeatureItem } from "../feature-item/feature-item";
import { getImageUrl } from "../../utils/utils";

const FEATURE_IMAGE_URL = getImageUrl('feature.png');
const FEATURES = [
  {
    img: "battery.svg",
    text: "Battery",
    description: "Battery 6.2V-AAC codec",
    link: "Learn More"
  },
  {
    img: "bluetooth.svg",
    text: "Bluetooth",
    description: "Battery 6.2V-AAC codec",
    link: "Learn More"
  },
  {
    img: "microphone.svg",
    text: "Microphone",
    description: "Battery 6.2V-AAC codec",
    link: "Learn More"
  }
]

export function addFeatureItems() {
  const featuresElem = document.getElementById("feature__items");

  FEATURES.forEach(feature => {
    const featureItem = FeatureItem(feature);
    featuresElem.appendChild(featureItem);
  });
}

const TEMPLATE = `
  <h2 class="feature__title">Good headphones and loud music is all you need</h2>
  <div class="feature__items" id="feature__items"></div>
  <div class="feature__image-cover">
     <img class="feature__image" src="${FEATURE_IMAGE_URL}">
  </div>
`;

export const Feature = () => {

  const featureElement = document.createElement('section');

  featureElement.classList.add('feature');
  featureElement.innerHTML = TEMPLATE;

  return featureElement;
};
