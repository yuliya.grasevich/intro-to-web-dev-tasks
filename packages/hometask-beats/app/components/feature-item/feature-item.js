import './feature-item.css';
import { getIconUrl } from "../../utils/utils";


const ELLIPSE_1_ICON_URL = getIconUrl('Ellipse-22.svg');
const ELLIPSE_2_ICON_URL = getIconUrl('Ellipse-24.svg');
const ELLIPSE_3_ICON_URL = getIconUrl('Ellipse-23.svg');
export function FeatureItem(data) {
  const featureItemElement = document.createElement('div');
  featureItemElement.classList.add("feature__item");

  featureItemElement.innerHTML=`
       <div class="feature__item__image-cover"
       style="background-image: url(${ELLIPSE_1_ICON_URL}), url(${ELLIPSE_2_ICON_URL}), url(${ELLIPSE_3_ICON_URL});">
          <img src="${getIconUrl(`${data.img}`)}" alt="feature image">
       </div>
       <div class="feature__item-params">
          <h4 class="feature__item-params-title">${data.text}</h4>
          <p class="feature__item-params-description">${data.description}</p>
          <a href="#" class="feature__item-params-link">${data.link}</a>
       </div>
  `;

  return featureItemElement;
}

