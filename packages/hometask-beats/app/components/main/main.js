import { Hero } from "../hero/hero";
import { Colour } from "../colour/colour";
import { Feature } from "../feature/feature";
import { Product } from "../product/product";
import { Boxing } from "../boxing/boxing";
import { Cta } from "../cta/cta";


export const Main = () => {

  const mainElement = document.createElement('main');
  mainElement.classList.add('main');
  mainElement.append(Hero());
  mainElement.append(Colour());
  mainElement.append(Feature());
  mainElement.append(Product());
  mainElement.append(Boxing());
  mainElement.append(Cta());

  return mainElement;
};



