import './product-item.css';
import { getIconUrl, getImageUrl } from "../../utils/utils";


const SHOPPING_CARD_ICON_URL = getIconUrl('shopping-cart.svg');
const STARS_IMAGE_URL = getImageUrl('stars.svg');
export function ProductItem(data) {
  const productItemElement = document.createElement('div');
  productItemElement.classList.add("product__item");

  productItemElement.innerHTML=`
    <div class="product__item-image-cover"
    style="background-color: ${data.colorBg};">
      <img class="product__item-image" src="${getImageUrl(`${data.img}`)}" alt="headphones">
    </div>
    <div class="product__card-icon"
    style="background-color: ${data.colorBg};">
      <img src="${SHOPPING_CARD_ICON_URL}" alt="shopping card">
    </div>
    <div class="product__item-data">
      <div class="product__item-data-row">
         <div class="product__item-data-rating">
             <img class="product__item-data-rating__image" src="${STARS_IMAGE_URL}" alt="stars">
        </div>
        <p class="product__item-data-rating__text">${data.rating}</p>
        </div>
        <div class="product__item-data-row">
         <p class="product__item-data-text">${data.name}</p>
         <p class="product__item-data-price">${data.price}</p>
        </div>
      </div>
  `;

  return productItemElement;
}
