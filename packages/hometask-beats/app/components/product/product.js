import './product.css';
import { ProductItem } from "../product-item/product-item";

const PRODUCTS = [
  {
    id: "3",
    img: "product-03.png",
    rating: "4.50",
    name: "Red Headphone",
    price: "$ 256",
    colorBg: "var(--color-pink)"
  },
  {
    id: "1",
    img: "product-02.png",
    rating: "4.50",
    name: "Blue Headphone",
    price: "$ 235",
    colorBg: "var(--color-blue)"
  },
  {
    id: "2",
    img: "product-01.png",
    rating: "4.50",
    name: "Green Headphone",
    price: "$ 245",
    colorBg: "var(--color-green)"
  }
]

export function addProductItem() {
  const productsElems = document.getElementById("product__cards");

  PRODUCTS.forEach(product => {
    const productItem = ProductItem(product);
    productItem.setAttribute("id",`product-item-${product.id}`);
    productsElems.appendChild(productItem);
  });
}

const TEMPLATE = `
  <h2 class="product__title">Our Latest Product</h2>
  <p class="product__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas facilisis nunc ipsum aliquam, ante.</p>
  <div class="product__cards" id="product__cards"></div>
`;

export const Product = () => {

  const productElement = document.createElement('section');

  productElement.classList.add('product');
  productElement.innerHTML = TEMPLATE;

  return productElement;
};
