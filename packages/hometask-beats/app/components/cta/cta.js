import './cta.css';


const TEMPLATE = `
<div class="cta__container">
   <h2 class="cta__title">
        Subscribe
   </h2>
   <p class="cta__text">
     Lorem ipsum dolor sit amet, consectetur
   </p>
   <form action="#" class="cta__form">
      <input type="email" placeholder="Enter Your email address" class="cta__form-input">
      <button type="submit" class="cta__form-submit">
         Subscribe
      </button>
   </form>
</div>
`;

export const Cta = () => {

  const ctaElement = document.createElement('section');

  ctaElement.classList.add('cta');
  ctaElement.innerHTML = TEMPLATE;

  return ctaElement;
};
