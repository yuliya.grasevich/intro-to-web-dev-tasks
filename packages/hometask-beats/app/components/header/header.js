import './header.css';
import { getIconUrl } from "../../utils/utils";

const LOGO_ICON_URL = getIconUrl('logo.svg');
const SEARCH_ICON_URL = getIconUrl('search.svg');
const BOX_ICON_URL = getIconUrl('box.svg');
const USER_ICON_URL = getIconUrl('user.svg');
const MENU_ICON_URL = getIconUrl('menu.svg');
const TEMPLATE = `
  <a href="#">
    <img class="header__logo-icon" src="${LOGO_ICON_URL}" alt="headphones">
  </a>
  <nav>
    <ul class="header__navigation">
        <li class="header__nav-item"><a href="#"><img src="${SEARCH_ICON_URL}" alt="search icon"></a></li>
        <li class="header__nav-separator"></li>
        <li class="header__nav-item"><a href="#"><img src="${BOX_ICON_URL}" alt="box icon"></a></li>
        <li class="header__nav-separator"></li>
        <li class="header__nav-item"><a href="#"><img src="${USER_ICON_URL}" alt="user icon"></a></li>
    </ul>
  </nav>
  <a href="#">
    <img class="header__menu-icon" src="${MENU_ICON_URL}" alt="menu burger">
  </a>
`;

export const Header = () => {

  const headerElement = document.createElement('header');

  headerElement.classList.add('header');
  headerElement.innerHTML = TEMPLATE;

  return headerElement;
};
