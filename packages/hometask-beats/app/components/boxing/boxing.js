import './boxing.css';
import { getImageUrl , getIconUrl } from "../../utils/utils";

const BOXING_IMAGE_URL = getImageUrl('boxing.png');
const ARROW_ICON_URL = getIconUrl('arrow-in-circle.svg');
const TEMPLATE = `
    <div class="boxing__image-cover">
        <img class="boxing__image" src="${BOXING_IMAGE_URL}" alt="headphones">
    </div>
    <div class="boxing__info">
         <h2 class="boxing__info-title">Whatever you get in the box</h2>
         <ul class="boxing__info-list">
            <li class="boxing__info-list__item"><img class="boxing__info-list__image" src="${ARROW_ICON_URL}" alt="arrow in circle">5A Charger</li>
            <li class="boxing__info-list__item"><img class="boxing__info-list__image" src="${ARROW_ICON_URL}" alt="arrow in circle">Extra battery</li>
            <li class="boxing__info-list__item"><img class="boxing__info-list__image" src="${ARROW_ICON_URL}" alt="arrow in circle">Sophisticated bag</li>
            <li class="boxing__info-list__item"><img class="boxing__info-list__image" src="${ARROW_ICON_URL}" alt="arrow in circle">User manual guide</li>
         </ul>
</div>
`;

export const Boxing = () => {

  const boxingElement = document.createElement('section');

  boxingElement.classList.add('boxing');
  boxingElement.innerHTML = TEMPLATE;

  return boxingElement;
};
