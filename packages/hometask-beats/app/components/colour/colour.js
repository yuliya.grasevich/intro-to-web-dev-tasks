import './colour.css';
import { getImageUrl , getIconUrl } from "../../utils/utils";

const COLOUR_1_IMAGE_URL = getImageUrl('clour-01.png');
const COLOUR_2_IMAGE_URL = getImageUrl('clour-02.png');
const COLOUR_3_IMAGE_URL = getImageUrl('clour-03.png');
const ARROW_LEFT_ICON_URL = getIconUrl('arrow-left.svg');
const ARROW_RIGHT_ICON_URL = getIconUrl('arrow-right.svg');
const TEMPLATE = `
  <h3 class="colour__title">Our Latest<br> colour collection 2021</h3>
  <div class="colour__options">
    <img class="colour__arrow-icon" src="${ARROW_LEFT_ICON_URL}" alt="arrow left">
    <div class="colour__image-cover">
        <img class="colour__image left-option" src="${COLOUR_2_IMAGE_URL}" alt="headphones">
    </div>
    <div class="colour__image-cover">
        <img class="colour__image central-option" src="${COLOUR_1_IMAGE_URL}" alt="headphones">
     </div>
     <div class="colour__image-cover">
        <img class="colour__image right-option" src="${COLOUR_3_IMAGE_URL}" alt="headphones">
    </div>
    <img class="colour__arrow-icon" src="${ARROW_RIGHT_ICON_URL}" alt="arrow right">
  </div>
`;

export const Colour = () => {

  const colourElement = document.createElement('section');

  colourElement.classList.add('colour');
  colourElement.innerHTML = TEMPLATE;

  return colourElement;
};
