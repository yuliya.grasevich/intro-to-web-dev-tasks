import './footer.css';
import { getIconUrl } from "../../utils/utils";

const LOGO_ICON_URL = getIconUrl('logo.svg');
const INSTAGRAM_ICON_URL = getIconUrl('instagram.svg');
const TWITTER_ICON_URL = getIconUrl('twitter.svg');
const FACEBOOK_ICON_URL = getIconUrl('facebook.svg');
const TEMPLATE = `
  <a href="#">
    <img class="footer__logo-icon" src="${LOGO_ICON_URL}" alt="headphones">
  </a>
  <nav>
    <ul class="footer__navigation">
        <li class="footer__nav-item"><a href="#">Home</a></li>
        <li class="footer__nav-item"><a href="#">About</a></li>
        <li class="footer__nav-item"><a href="#">Product</a></li>
    </ul>
  </nav>
  <div>
    <ul class="footer__social-links">
        <li class="footer__social-links-item"><a href="#"><img src="${INSTAGRAM_ICON_URL}" alt="instagram icon"></a></li>
        <li class="footer__social-links-item"><a href="#"><a href="#"><img src="${TWITTER_ICON_URL}" alt="twitter icon"></a></li>
        <li class="footer__social-links-item"><a href="#"><a href="#"><img src="${FACEBOOK_ICON_URL}" alt="facebook icon"></a></li>
    </ul>
  </div>
`;

export const Footer = () => {

  const footerElement = document.createElement('footer');

  footerElement.classList.add('footer');
  footerElement.innerHTML = TEMPLATE;

  return footerElement;
};

