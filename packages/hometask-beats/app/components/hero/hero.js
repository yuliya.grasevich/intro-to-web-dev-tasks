import './hero.css';
import { getImageUrl } from "../../utils/utils";

const HERO_IMAGE_URL = getImageUrl('hero.png');
const TEMPLATE = `
<div class="hero__image-cover">
    <img class="hero__image" src="${HERO_IMAGE_URL}" alt="headphones">
</div>

  <div class="hero__content">
    <h2 class="hero__subtitle">Hear it. Feel it</h2>
    <h1 class="hero__title">Move with the music</h1>
    <div class="hero__offer">
        <span class="hero__offer-cut-price">$ 435</span>
        <span class="hero__offer-separator"></span>
        <span class="hero__offer-full-price">$ 465</span>
    </div>
    <button class="hero__button" type="button">BUY NOW</button>
  </div>
`;

export const Hero = () => {

  const heroElement = document.createElement('section');

  heroElement.classList.add('hero');
  heroElement.innerHTML = TEMPLATE;

  return heroElement;
};
