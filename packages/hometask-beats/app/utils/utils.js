export function getIconUrl(icon) {
  return new URL(`/assets/icons/${icon}`, import.meta.url).href;
}

export function getImageUrl(image) {
  return new URL(`/assets/images/${image}`, import.meta.url).href;
}
