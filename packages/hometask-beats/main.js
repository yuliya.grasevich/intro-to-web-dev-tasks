import { Header } from './app/components/header/header';
import { Footer } from './app/components/footer/footer';
import { Main } from "./app/components/main/main";
import { addFeatureItems } from "./app/components/feature/feature";
import { addProductItem } from "./app/components/product/product";

setTimeout(() => {
  document.querySelector('#app').prepend(Header());
  document.querySelector('#app').append(Main());
  addFeatureItems();
  addProductItem();
  document.querySelector('#app').append(Footer());
});
