import './landing.css';
import { getImageUrl } from "../../utils/utils";


const GIRL_IMAGE_URL = getImageUrl('background-page-landing.png');
const TEMPLATE = `
<section class="landing">
   <div class="landing__content">
       <h1 class="landing__content-title">Feel the music</h1>
       <p class="landing__content-text">Stream over 10 million songs with one click</p>
       <a href="#/sign-up"><button class="button">Join now</button></a>
   </div>
   <div class="landing-image" style="background-image: url(${GIRL_IMAGE_URL});"></div>
</section>
  `;


export const LandingPage = () => TEMPLATE;
