import './feature.css';
import { getImageUrl } from "../../utils/utils";


const MUSIC_TITLES_IMAGE_URL = getImageUrl('music-titles.png');
const TEMPLATE = `
<section class="feature">
   <div class="feature__content">
       <h1 class="feature__content-title">Discover new music</h1>
       <div class="feature__content-navigation">
           <button class="feature__content-navigation__item button">Charts</button>
           <button class="feature__content-navigation__item button">Songs</button>
           <button class="feature__content-navigation__item button">Artists</button>
        </div>
       <p class="feature__content-text">By joing you can benefit by listening to the latest albums released</p>
   </div>
   <div class="feature-image" style="background-image: url(${MUSIC_TITLES_IMAGE_URL});"></div>
</section>
  `;


export const FeaturePage = () => TEMPLATE;
