import './sign-up.css';
import { getImageUrl } from "../../utils/utils";


const GIRL_IMAGE_URL = getImageUrl('background-page-sign-up.png');
const TEMPLATE = `
<section class="sign-up">
   <div class="sign-up__form-container">
       <form class="sign-up__form">
             <div class="sign-up__form-wrapper">
              <label for="name">Name:</label>
              <input type="text" id="name" name="name">
             </div>
             <div class="sign-up__form-wrapper">
              <label for="password">Password:</label>
              <input type="password" id="password" name="password">
             </div>
             <div class="sign-up__form-wrapper">
              <label for="email">e-mail:</label>
              <input type="email" id="email" name="email">
             </div>
           </form>
       <button type="submit" class="button">Join now</button>
   </div>
   <div class="sign-up-image" style="background-image: url(${GIRL_IMAGE_URL});"></div>
</section>
  `;


export const SignUpPage = () => TEMPLATE;
