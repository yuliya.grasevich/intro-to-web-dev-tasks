import './not-found.css';
import { getImageUrl } from "../../utils/utils";


const ERROR_IMAGE_URL = getImageUrl('error.png');
const TEMPLATE = `
<section class="not-found">
   <img class="error-image" src="${ERROR_IMAGE_URL}" alt="error">
   <p>Sorry, page in NOT found.</p>
</section>
  `;


export const NotFoundPage = () => TEMPLATE;
