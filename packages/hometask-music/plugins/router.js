import { Router } from 'yourrouter';
import { LandingPage } from '../pages/landing/landing';
import { SignUpPage } from "../pages/sign-up/sign-up";
import { NotFoundPage } from "../pages/not-found/not-found";
import { FeaturePage } from "../pages/feature/feature";

Router.createInstance({
  renderId: '#routes',
  path404: '/not-found',
});

const router = Router.getInstance();

router.addRoute('/', () => LandingPage);

router.addRoute('/sign-up', () => SignUpPage);

router.addRoute('/feature', () => FeaturePage);

router.addRoute('/not-found', () => NotFoundPage);


export default router;
