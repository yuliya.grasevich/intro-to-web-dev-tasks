import { onDiscoverMenuClicked } from "../components/header/header";

export const Controller = () => {
  const menuButtonElement = document.querySelector('#discover-menu');
  menuButtonElement.addEventListener('click', onDiscoverMenuClicked);
}
