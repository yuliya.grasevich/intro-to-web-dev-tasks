export function getImageUrl(img) {
  return new URL(`/assets/images/${img}`, import.meta.url).href;
}

export function getIconUrl(icon) {
  return new URL(`/assets/icons/${icon}`, import.meta.url).href;
}
