import './footer.css';


export const Footer = () => {
  const footer = document.createElement('footer');

  footer.classList.add('footer');

  footer.innerHTML = `
    <nav class="footer-list">
        <ul class="footer-list__body">
          <li class="footer-list__item"><a>About Us</a></li>
          <li class="footer-list__item"><a>Contact</a></li>
          <li class="footer-list__item"><a>CR Info</a></li>
          <li class="footer-list__item"><a>Twitter</a></li>
          <li class="footer-list__item"><a>Facebook</a></li>
        </ul>
    </nav>
    <div class="footer-tag">Wlodzimierz Simon © 2022</div>
    `;

  return footer;
};
