import './header.css';
import { getIconUrl } from "../../utils/utils";


const LOGO_ICON_URL = getIconUrl('logo.svg');
const TEMPLATE = `
    <div class="header__logo-image__container">
        <img src="${LOGO_ICON_URL}" alt="logo">
    </div>
    <nav id="menu">
        <ul class="header-list__body">
          <li class="header-list__item"><a>Simo</a></li>
          <li class="header-list__item" id="discover-menu"><a>Discover</a></li>
          <li class="header-list__item" id="join-menu"><a href="#/sign-up">Join</a></li>
          <li class="header-list__item"><a>Sign In</a></li>
        </ul>
    </nav>
  `;


export const Header = () => {

  const headerElement = document.createElement('header');

  headerElement.classList.add('header');
  headerElement.innerHTML = TEMPLATE;

  return headerElement;
};

export function onDiscoverMenuClicked() {
  const currentPage = document.querySelector('#routes').children[0].classList[0];

  if (currentPage === 'landing') {
    window.location.replace("#/feature");
  } else if (currentPage === 'sign-up') {
    window.location.replace("#/");
  }

}
